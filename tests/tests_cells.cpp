#include "catch.hpp"
#include "cells/cell.hpp"

TEST_CASE("Rules of survival", "[rules]") {
    gol::Cell cell(gol::State::ALIVE);
    SECTION("Dies of starvation 0 neighbour") {
        cell.override_living_neighbours(0);
        cell.update();
        CHECK(cell.will_be_alive() == false);
    }

    SECTION("Dies of starvation 1 neighbour") {
        cell.override_living_neighbours(1);
        cell.update();
        CHECK(cell.will_be_alive() == false);
    }

    SECTION("Survives with 2 neighbours") {
        cell.override_living_neighbours(2);
        cell.update();
        CHECK(cell.will_be_alive() == true);
    }

    SECTION("Survives with 3 neighbours") {
        cell.override_living_neighbours(3);
        cell.update();
        CHECK(cell.will_be_alive() == true);
    }

    SECTION("Dies of overpop 4 neighbours") {
        cell.override_living_neighbours(4);
        cell.update();
        CHECK(cell.will_be_alive() == false);
    }

    SECTION("Dies of overpop 5 neighbours") {
        cell.override_living_neighbours(5);
        cell.update();
        CHECK(cell.will_be_alive() == false);
    }

    SECTION("Dies of overpop 6 neighbours") {
        cell.override_living_neighbours(6);
        cell.update();
        CHECK(cell.will_be_alive() == false);
    }

    SECTION("Dies of overpop 7 neighbours") {
        cell.override_living_neighbours(7);
        cell.update();
        CHECK(cell.will_be_alive() == false);
    }

    SECTION("Dies of overpop 8 neighbours") {
        cell.override_living_neighbours(8);
        cell.update();
        CHECK(cell.will_be_alive() == false);
    }
}

TEST_CASE("Rules of creation", "[rules]") {
    gol::Cell cell(gol::State::DEAD);
    SECTION("Stays dead if 0 neighbour") {
        cell.override_living_neighbours(0);
        cell.update();
        CHECK(cell.will_be_alive() == false);
    }

    SECTION("Stays dead if 1 neighbour") {
        cell.override_living_neighbours(1);
        cell.update();
        CHECK(cell.will_be_alive() == false);
    }

    SECTION("Stays dead if 2 neighbours") {
        cell.override_living_neighbours(2);
        cell.update();
        CHECK(cell.will_be_alive() == false);
    }

    SECTION("Birth with 3 neighbours") {
        cell.override_living_neighbours(3);
        cell.update();
        CHECK(cell.will_be_alive() == true);
    }

    SECTION("Stays dead if 4 neighbours") {
        cell.override_living_neighbours(4);
        cell.update();
        CHECK(cell.will_be_alive() == false);
    }

    SECTION("Stays dead if 5 neighbours") {
        cell.override_living_neighbours(5);
        cell.update();
        CHECK(cell.will_be_alive() == false);
    }

    SECTION("Stays dead if 6 neighbours") {
        cell.override_living_neighbours(6);
        cell.update();
        CHECK(cell.will_be_alive() == false);
    }

    SECTION("Stays dead if 7 neighbours") {
        cell.override_living_neighbours(7);
        cell.update();
        CHECK(cell.will_be_alive() == false);
    }

    SECTION("Stays dead if 8 neighbours") {
        cell.override_living_neighbours(8);
        cell.update();
        CHECK(cell.will_be_alive() == false);
    }
}
