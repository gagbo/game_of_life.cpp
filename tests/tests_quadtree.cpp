#include "catch.hpp"
#include "cells/quadtree.hpp"

TEST_CASE("Point navigation follows Computer Graphics conventions", "[Point]") {
  /*
    NW  N  NE   (0,0) -> x+
    W   x   E     v
    SW  S  SE     y+
   */
  using namespace gol::tree;
  CHECK(Point(0, 0).NW() == Point(-1, -1));
  CHECK(Point(0, 0).N() == Point(0, -1));
  CHECK(Point(0, 0).NE() == Point(1, -1));
  CHECK(Point(0, 0).E() == Point(1, 0));
  CHECK(Point(0, 0).SE() == Point(1, 1));
  CHECK(Point(0, 0).S() == Point(0, 1));
  CHECK(Point(0, 0).SW() == Point(-1, 1));
  CHECK(Point(0, 0).W() == Point(-1, 0));
}

TEST_CASE("AABB contains its origin", "[AABB]") {
  gol::tree::Point center(22, 23);
  SECTION("Half-dimension of 1") {
    gol::tree::AABB box(center, 1);
    CHECK(box.contains_point(center) == true);
  }
  SECTION("Half-dimension of 0") {
    gol::tree::AABB box(center, 0);
    CHECK(box.contains_point(center) == true);
  }
}

TEST_CASE("AABB contains_point edge cases", "[AABB]") {
  gol::tree::Point center(-12, 23);

  SECTION("Half-dimension of 2 - side") {
    gol::tree::AABB box(center, 2);
    CHECK(box.contains_point(center.N()) == true);
  }
  SECTION("Half-dimension of 2 - side->side") {
    gol::tree::AABB box(center, 2);
    CHECK(box.contains_point(center.S().S()) == true);
  }
  SECTION("Half-dimension of 2 - side->corner") {
    gol::tree::AABB box(center, 2);
    CHECK(box.contains_point(center.W().NW()) == true);
  }
  SECTION("Half-dimension of 2 - corner->corner") {
    gol::tree::AABB box(center, 2);
    CHECK(box.contains_point(center.SW().SW()) == true);
  }

  SECTION("Half-dimension of 1 - side") {
    gol::tree::AABB box(center, 1);
    CHECK(box.contains_point(center.N()) == true);
  }
  SECTION("Half-dimension of 1 - side->side") {
    gol::tree::AABB box(center, 1);
    CHECK(box.contains_point(center.N().N()) == false);
  }
  SECTION("Half-dimension of 1 - corner") {
    gol::tree::AABB box(center, 1);
    CHECK(box.contains_point(center.NW()) == true);
  }

  SECTION("Half-dimension of 0 - side") {
    gol::tree::AABB box(center, 0);
    CHECK(box.contains_point(center.W()) == false);
  }
  SECTION("Half-dimension of 0 - corner") {
    gol::tree::AABB box(center, 0);
    CHECK(box.contains_point(center.SE()) == false);
  }
}

TEST_CASE("AABB intersections", "[AABB]") {
  using namespace gol::tree;
  SECTION("Same coordinates intersect") {
    Point center(37, -1);
    AABB box1(center, 2);
    AABB box2(center, 2);
    CHECK(box1.intersects_aabb(box2) == true);
  }

  SECTION("Included AABB intersect - same center") {
    Point center(-3, -1);
    AABB box1(center, 2);
    AABB box2(center, 1);
    CHECK(box1.intersects_aabb(box2) == true);
    CHECK(box2.intersects_aabb(box1) == true);
  }

  SECTION("Included AABB intersect - different center") {
    Point center(-3, -1);
    Point center2(center.NW());
    AABB box1(center, 2);
    AABB box2(center2, 0);
    CHECK(box1.intersects_aabb(box2) == true);
    CHECK(box2.intersects_aabb(box1) == true);
  }

  SECTION("Far apart AABB do not intersect") {
    Point center1(-3, -1);
    Point center2(123, -1);
    AABB box1(center1, 3);
    AABB box2(center2, 4);
    CHECK(box1.intersects_aabb(box2) == false);
    CHECK(box2.intersects_aabb(box1) == false);
  }

  SECTION("Touching AABB - no intersect on sides") {
    Point center1(-3, 1);
    Point center2(center1.N().N().N());
    AABB box1(center1, 2);
    AABB box2(center2, 0);
    CHECK(box1.intersects_aabb(box2) == false);
    CHECK(box2.intersects_aabb(box1) == false);
  }

  SECTION("Touching AABB - no intersect on corners") {
    Point center1(-3, 1);
    Point center2(center1.SW().SW().SW().SW());
    AABB box1(center1, 2);
    AABB box2(center2, 1);
    CHECK(box1.intersects_aabb(box2) == false);
    CHECK(box2.intersects_aabb(box1) == false);
  }

  SECTION("Touching AABB - intersect on corners") {
    Point center1(12, 40);
    Point center2(center1.SE().SE().SE().SE());
    AABB box1(center1, 3);
    AABB box2(center2, 2);
    CHECK(box1.intersects_aabb(box2) == true);
    CHECK(box2.intersects_aabb(box1) == true);
  }

  SECTION("Touching AABB - intersect on sides") {
    Point center1(-3, 1);
    Point center2(center1.E().E().E());
    AABB box1(center1, 2);
    AABB box2(center2, 2);
    CHECK(box1.intersects_aabb(box2) == true);
    CHECK(box2.intersects_aabb(box1) == true);
  }
}

TEST_CASE("QuadTree interface", "[Quadtree]") {
  using namespace gol::tree;
  Point center(0, 0);
  AABB root_boundary(center, 400);
  QuadTree<Point> root(root_boundary);

  SECTION("Outside points are not inserted") {
    CHECK(root.insert(Point(892, 432)) == false);
  }

  SECTION("Point is inserted and found in query_range") {
    int point_x = 233;
    int point_y = -120;
    CHECK(root.insert(Point(point_x, point_y)) == true);
    auto point_list = root.query_range(root_boundary);
    CHECK(point_list.size() == 1);
    CHECK(point_list[0].x() == Approx(point_x));
    CHECK(point_list[0].y() == Approx(point_y));
  }

  SECTION("Query range returns empty list when range is outside") {
    AABB bad_boundary(Point(-8000000, -8000000), 1.0);
    CHECK(root.query_range(bad_boundary).empty() == true);
  }

  SECTION("Add 100 points and check they're all found") {
    for (int i = 0; i < 10; ++i) {
      for (int j = 0; j < 10; ++j) {
        CHECK(root.insert(Point(10 * i, 10 * j)) == true);
      }
    }
    CHECK(root.query_range(root_boundary).size() == 100);
  }

  SECTION("Add 100 points and check they're all found - close dots") {
    for (int i = 0; i < 10; ++i) {
      for (int j = 0; j < 10; ++j) {
        CHECK(root.insert(Point(i, j)) == true);
      }
    }
    CHECK(root.query_range(root_boundary).size() == 100);
  }

  SECTION("Only unique points are added") {
    CHECK(root.insert(Point(-2, 3)) == true);
    CHECK(root.insert(Point(-2, 3)) == false);
    CHECK(root.insert(Point(-2, 3)) == false);
    CHECK(root.query_range(root_boundary).size() == 1);
  }

  SECTION("Only Unique points are added - close dots") {
    for (int i = 0; i < 10; ++i) {
      for (int j = 0; j < 10; ++j) {
        REQUIRE(root.insert(Point(i, j)) == true);
      }
    }
    CHECK(root.insert(Point(5, 7)) == false);
    CHECK(root.insert(Point(5, 7)) == false);
    CHECK(root.insert(Point(5, 7)) == false);
    CHECK(root.insert(Point(5, 7)) == false);
    CHECK(root.query_range(root_boundary).size() == 100);
  }

  SECTION("Remove points") {
    for (int i = 0; i < 10; ++i) {
      for (int j = 0; j < 10; ++j) {
          REQUIRE(root.insert(Point(10 * i, 10 * j)) == true);
      }
    }
    CHECK(root.remove(Point(80, 10)) == true);
    CHECK(root.query_range(root_boundary).size() == 99);
    CHECK(root.remove(Point(80, 10)) == false);
    CHECK(root.remove(Point(120.1f, 120.1f)) == false);
    CHECK(root.remove(Point(35, 35)) == false);
  }

  SECTION("Remove points - close dots") {
    for (int i = 0; i < 10; ++i) {
      for (int j = 0; j < 10; ++j) {
        REQUIRE(root.insert(Point(i, j)) == true);
      }
    }
    CHECK(root.remove(Point(6, 7)) == true);
    CHECK(root.query_range(root_boundary).size() == 99);
    CHECK(root.remove(Point(6, 7)) == false);
    CHECK(root.remove(Point(5, 7)) == true);
    CHECK(root.query_range(root_boundary).size() == 98);
    CHECK(root.remove(Point(5, 7)) == false);
    CHECK(root.remove(Point(8, 2.1f)) == false);
    CHECK(root.remove(Point(7.99f, 5)) == false);
  }
}
