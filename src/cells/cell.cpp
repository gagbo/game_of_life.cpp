#include "cell.hpp"

namespace gol {
Cell::Cell()
{

}

int Cell::count_live_neighbours() const
{
    alive_neighbours = 0;
    for (auto neighbour_to_lock : neighbours) {
        auto neighbour = neighbour_to_lock.lock();
        if (neighbour && neighbour->is_alive()) {
            alive_neighbours++;
        }
    }

    return alive_neighbours;
}

void Cell::update() {
    if (currently_alive) {
        update_living_cell();
    } else {
        update_dead_cell();
    }
}

void Cell::update_dead_cell() {
    next_state = (alive_neighbours == 3);
}

void Cell::update_living_cell() {
    next_state = (alive_neighbours == 2 || alive_neighbours == 3);
}

} // namespace gol
