#ifndef GOLCELL_H
#define GOLCELL_H
#include "quadtree.hpp"
#include <memory>

namespace gol {
enum State : bool { DEAD = false, ALIVE = true };

class Cell : public tree::Point {
public:
  Cell();
  Cell(bool _state) : Point(), currently_alive(_state) {}
  Cell(float _x, float _y, bool _state = State::DEAD)
      : Point(_x, _y), currently_alive(_state) {}
  void update();
  inline bool is_alive() const { return currently_alive; }
  inline bool will_be_alive() const { return next_state; }

  //! For testing purposes only
  inline void override_living_neighbours(int new_count) const {
    alive_neighbours = new_count;
  }

protected:
  int count_live_neighbours() const;
  void update_living_cell();
  void update_dead_cell();

private:
  //! True if alive
  bool next_state{false};

  //! True if alive
  bool currently_alive{false};

  mutable int alive_neighbours{0};

  std::weak_ptr<Cell> neighbours[8];
};
} // namespace gol

#endif // GOLCELL_H
