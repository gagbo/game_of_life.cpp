#ifndef GOL_CELLS_BOARD_H
#define GOL_CELLS_BOARD_H

#include <memory>
#include "cell.hpp"

namespace gol {

    class Board
    {
    public:
        Board();
        ~Board() {}
    protected:
    private:
    };

}  // namespace gol

#endif // GOL_CELLS_BOARD_H
