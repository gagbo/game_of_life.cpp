#include "quadtree.hpp"
#include <cmath>
#include <iostream>

namespace gol {
namespace tree {

Point Point::NW(float step) const {
  return Point(x_coord - step, y_coord - step);
}
Point Point::N(float step) const { return Point(x_coord, y_coord - step); }
Point Point::NE(float step) const {
  return Point(x_coord + step, y_coord - step);
}
Point Point::E(float step) const { return Point(x_coord + step, y_coord); }
Point Point::SE(float step) const {
  return Point(x_coord + step, y_coord + step);
}
Point Point::S(float step) const { return Point(x_coord, y_coord + step); }
Point Point::SW(float step) const {
  return Point(x_coord - step, y_coord + step);
}
Point Point::W(float step) const { return Point(x_coord - step, y_coord); }

float AABB::bottom_incl() const { return center.y() + half_dim; }
float AABB::top_incl() const { return center.y() - half_dim; }
float AABB::left_incl() const { return center.x() - half_dim; }
float AABB::right_incl() const { return center.x() + half_dim; }

bool AABB::contains_point(const Point &target) const {
  float diff_x = std::abs(target.x() - center.x());
  float diff_y = std::abs(target.y() - center.y());
  return (diff_x <= half_dim && diff_y <= half_dim);
}

bool AABB::intersects_aabb(const AABB &other) const {
  if (other.bottom_incl() < top_incl()) {
    return false;
  }
  if (other.top_incl() > bottom_incl()) {
    return false;
  }
  if (other.right_incl() < left_incl()) {
    return false;
  }
  if (other.left_incl() > right_incl()) {
    return false;
  }
  return true;
}
} // namespace tree
} // namespace gol
