#ifndef GOL_CELLS_QUADTREE_H_
#define GOL_CELLS_QUADTREE_H_

#include <cmath>
#include <iostream>
#include <memory>
#include <set>
#include <tuple>
#include <type_traits>
#include <vector>

namespace gol {
namespace tree {
class Point {
public:
  Point(float _x = 0.0f, float _y = 0.0f) : x_coord(_x), y_coord(_y) {}
  float x() const { return x_coord; }
  float y() const { return y_coord; }
  Point NW(float step = 1.0f) const;
  Point N(float step = 1.0f) const;
  Point NE(float step = 1.0f) const;
  Point E(float step = 1.0f) const;
  Point SE(float step = 1.0f) const;
  Point S(float step = 1.0f) const;
  Point SW(float step = 1.0f) const;
  Point W(float step = 1.0f) const;
  bool operator==(const Point &other) const {
    return std::tie(x_coord, y_coord) == std::tie(other.x_coord, other.y_coord);
  }
  bool operator<(const Point &other) const {
    return std::tie(x_coord, y_coord) < std::tie(other.x_coord, other.y_coord);
  }

protected:
  float x_coord;
  float y_coord;
};

class AABB {
public:
  AABB(Point _center, float _half_dim) : center(_center), half_dim(_half_dim) {}
  AABB(){};
  bool contains_point(const Point &target) const;
  bool intersects_aabb(const AABB &other) const;

  float bottom_incl() const;
  float top_incl() const;
  float left_incl() const;
  float right_incl() const;
  float half_d() const { return half_dim; }
  Point center_point() const { return center; }

protected:
  Point center{0.0f, 0.0f};
  float half_dim{1.0f};
};

template <class T> class QuadTree {
  static_assert(std::is_base_of<Point, T>::value, "T must derive from Point");

public:
  static const int NODE_CAPACITY = 4;
  QuadTree() {}
  QuadTree(AABB _boundary) : boundary(_boundary) {}
  ~QuadTree() {}

  //! Returns true if the point was inserted
  bool insert(const T &p);
  //! Returns true if a point was removed
  bool remove(const T &p);
  std::vector<T> query_range(const AABB &range) const;

  template <class U>
  friend std::ostream &operator<<(std::ostream &os, const QuadTree<U> &c);

protected:
  std::set<T> points;
  AABB boundary{Point(0.0f, 0.0f), 0.0f};
  std::unique_ptr<QuadTree<T>> north_west{nullptr};
  std::unique_ptr<QuadTree<T>> north_east{nullptr};
  std::unique_ptr<QuadTree<T>> south_west{nullptr};
  std::unique_ptr<QuadTree<T>> south_east{nullptr};
  void subdivide();
};

} // namespace tree
} // namespace gol

#include "quadtree.tpp"

#endif // GOL_CELLS_QUADTREE_H_
