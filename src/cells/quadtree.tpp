// File included from quadtree.hpp
namespace gol {
namespace tree {

template <class T>
std::ostream &operator<<(std::ostream &os, const QuadTree<T> &c) {
  os << c.boundary.center_point().x() << " ; " << c.boundary.center_point().y()
     << " radius " << c.boundary.half_d() << ": " << c.points.size()
     << " points in the Node :\n";
  for (auto p : c.points) {
    os << "  " << p.x() << " ; " << p.y() << "\n";
  }

  if (c.north_west) {
    os << "NW : " << *(c.north_west);
    os << "NE : " << *(c.north_east);
    os << "SW : " << *(c.south_west);
    os << "SE : " << *(c.south_east);
  }

  return os;
}

template <class T> bool QuadTree<T>::insert(const T &p) {
  if (!boundary.contains_point(p)) {
    return false;
  }

  if (query_range(AABB(p, 0)).size() > 0) {
    return false;
  }

  if (points.size() < NODE_CAPACITY) {
    auto insertion = points.insert(p);
    return insertion.second;
  }

  if (!north_west) {
    subdivide();
  }

  if (north_west->insert(p)) {
    return true;
  }
  if (north_east->insert(p)) {
    return true;
  }
  if (south_west->insert(p)) {
    return true;
  }
  if (south_east->insert(p)) {
    return true;
  }

  return false;
}

template <class T> bool QuadTree<T>::remove(const T &p) {
  if (!boundary.contains_point(p)) {
    return false;
  }

  auto points_removed = points.erase(p);
  if (points_removed > 0) {
    return true;
  }

  if (!north_west) {
    return false;
  }

  // Evaluation stops at first true or last false
  return north_west->remove(p) || north_east->remove(p) ||
         south_west->remove(p) || south_east->remove(p);
}

template <class T> void QuadTree<T>::subdivide() {
  float quarter_dim = boundary.half_d() / 2;
  north_west = std::make_unique<QuadTree<T>>(
      AABB(boundary.center_point().NW(quarter_dim), quarter_dim));
  north_east = std::make_unique<QuadTree<T>>(
      AABB(boundary.center_point().NE(quarter_dim), quarter_dim));
  south_west = std::make_unique<QuadTree<T>>(
      AABB(boundary.center_point().SW(quarter_dim), quarter_dim));
  south_east = std::make_unique<QuadTree<T>>(
      AABB(boundary.center_point().SE(quarter_dim), quarter_dim));
}

template <class T>
std::vector<T> QuadTree<T>::query_range(const AABB &range) const {
  std::vector<T> points_in_range;

  if (!boundary.intersects_aabb(range)) {
    return points_in_range;
  }

  // TODO: <algorithm> to the rescue ?
  for (auto p : points) {
    if (range.contains_point(p)) {
      points_in_range.push_back(p);
    }
  }

  if (!north_west) {
    return points_in_range;
  }

  auto nw_range = north_west->query_range(range);
  points_in_range.insert(points_in_range.end(), nw_range.begin(),
                         nw_range.end());
  auto ne_range = north_east->query_range(range);
  points_in_range.insert(points_in_range.end(), ne_range.begin(),
                         ne_range.end());
  auto sw_range = south_west->query_range(range);
  points_in_range.insert(points_in_range.end(), sw_range.begin(),
                         sw_range.end());
  auto se_range = south_east->query_range(range);
  points_in_range.insert(points_in_range.end(), se_range.begin(),
                         se_range.end());

  return points_in_range;
}
} // namespace tree
} // namespace gol
