#include "golConfig.hpp"
#include "ui/mainwindow.hpp"
#include <QApplication>
#include <iostream>

int main(int argc, char *argv[]) {
  std::cerr << "Game of Life - Version " << GOL_VERSION_MAJOR << "."
            << GOL_VERSION_MINOR << "\n";
  QApplication a(argc, argv);
  MainWindow w;
  w.show();

  return a.exec();
}
